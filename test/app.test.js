const Cube = require('../src/app').Cube;
const Triangle = require('../src/app').Triangle;
const Cercle = require('../src/app').Cercle;
const expect = require('chai').expect;

describe('Testing the Cube Functions', function() {
    it('1. The side length of the Cube', function(done) {
        let c1 = new Cube(2);
        expect(c1.getSideLength()).to.equal(2);
        done();
    });
    
    it('2. The surface area of the Cube', function(done) {
        let c2 = new Cube(5);
        expect(c2.getSurfaceArea()).to.equal(150);
        done();
    });
    
    it('3. The volume of the Cube', function(done) {
        let c3 = new Cube(7);
        expect(c3.getVolume()).to.equal(343);
        done();
    });
    
});

describe('Testing the Triangle Functions', function(){

    // Triangle(AB,AC,BC)
    // et possède également un attribut est possible 
    it('1. Possible Triangle', function(done){
        let t1 = new Triangle(4,2,3)
        expect(t1.getExist()).to.equal(true);
        done();
    })
    it('2. Impossible Triangle', function(done){
        let t2 = new Triangle(1,2,3)
        expect(t2.getExist()).to.equal(false);
        done();
    })
    it('3 is Triangle Isocele', function(done){
        let t3 = new Triangle(2,3,2)
        expect(t3.isIsocele()).to.equal(true);
        done();
    })
    it('4 is Triangle Equilateral', function(done){
        let t4 = new Triangle(4,4,4)
        expect(t4.isEquilateral()).to.equal(true);
        done();
    })
    it('5 is Triangle Rectangle', function(done){
        let t5 = new Triangle(5,3,4)
        expect(t5.isRectangle()).to.equal(true);
        done();
    })

    it('6. The side lenght of the Triangle', function(done){
        let t6 = new Triangle(6,5,7)
        expect(t6.getSideLength()).to.equal(18);
        done();
    })
    it('7. The surface area of the Triangle', function(done) {
        let t7 = new Triangle(5,3,4)
        expect(t7.getSurfaceArea()).to.equal(6)
        done();
    });
    it('8. Try create Triangle with letter', function(done) {
        let t = new Triangle('a',3,4)
        expect(t.getExist()).to.equal(false)
        done();
    });
    it('9. Try create Triangle with negative number', function(done) {
        let t = new Triangle(-1,3,4)
        expect(t.getExist()).to.equal(false)
        done();
    });
    it('10. Try create Triangle with 0', function(done) {
        let t = new Triangle(0,3,4)
        expect(t.getExist()).to.equal(false)
        done();
    });
    
    describe('Testing the Circle Functions', function(){
        it('1. The Perimrter of the Circle', function(done) {
            let c1 = new Cercle(2);
            expect(c1.getPerimeter()).to.equal(2*Math.PI*2);
            done();
        });
        
        it('2. The surface area of the Circle', function(done) {
            let c2 = new Cercle(5);
            expect(c2.getSurfaceArea()).to.equal(Math.PI*5*5);
            done();
        });
        it('3. Try create Circle with letter', function(done) {
            let c = new Cercle('a')
            expect(c.getExist()).to.equal(false)
            done();
        });
        it('4. Try create Circle with negative number', function(done) {
            let c = new Cercle(-1)
            expect(c.getExist()).to.equal(false)
            done();
        });
        
    });



})