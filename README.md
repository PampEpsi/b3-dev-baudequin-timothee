# b3-dev-baudequin-timothee

# TDD Triangle 

Pour cet exercice avec le principe du TDD, j'ai commencé à me demander si tous les triangles étaient possibles.
J'ai donc commencé par écrire un test qui vérifie si un triangle est possible ou non.
Ensuite un triangle peut avoir des propriétés, j'ai donc écrit un test qui vérifie si un triangle est équilatéral, isocèle ou rectangle.
Enfin, j'ai écrit 2 tests qui test les fonctions qui calculent la surface et le périmètre d'un triangle.

J'ai ensuite développé la classe triangle avec ces méthodes.

Je me suis rendu compte que j'avais oublié de prendre en compte le cas où l'utilisateur essaye de créer un triangle avec des côtés avec autre chose qu'un nombre.
J'ai donc écrit 3 nouveaux test qui vérifie si l'utilisateur entre bien un nombre strictement positif et j'ai modifié la classe triangle pour que le triangle n'existe pas si l'utilisateur essaye de créer un triangle avec autre chose qu'un nombre. 

# BDD Cercle

Pour cet exercice avec le principe du BDD, j'ai commencé par dévlopper les fonctionnalités de base d'un cercle (création, calcul de périmètre et de surface).

Ensuite, j'ai écrit des tests qui vérifient si un cercle est possible ou non, donc il ne peut pas avoir un rayon négatif, nul ou avec d'autres valeurs que des nombres.

Enfin, j'ai écrit des tests qui vérifient le calcul de la surface et du périmètre d'un cercle et s'il est bien impossible de créer un cercle avec des nombres négatif ou avec des lettres.