class Cube {
    constructor(length) {
        this.length = length;
    }

    getSideLength() {
        return this.length;
    }

    getSurfaceArea() {
        return (this.length * this.length) * 6;
    }

    getVolume() {
        return Math.pow(this.length, 3);
    }
}

class Triangle {
    constructor(AB, AC, BC) {
        // par degault le triangle est impossible
        this.exist = false;
        // il devient possible si les 3 cotés sont des nombres
        if (typeof AB == "number" & typeof AC == "number" & typeof BC == "number") {
            this.exist = true;
        }
        // on assigne les valeurs des cotés si les 3 cotés sont des nombres supérieurs à 0
        if (AB > 0 & AC > 0 & BC > 0) {
            this.ab = AB;
            this.ac = AC;
            this.bc = BC;
        }
        // si les cotes sont des nombres négatifs ou nuls, le triangle est impossible
        else {
            this.exist = false;
        }
        // si l'un des cotes est plus grand que la somme des deux autres, le triangle est impossible
        if (AB >= AC + BC | AC >= AB + BC | BC >= AC + AB) {
            this.exist = false;
        }
    }

    getExist() {
        return this.exist;
    }
    isIsocele() {
        if (this.exist == false) {
            return false;
        }
        if (this.ab == this.ac | this.ab == this.bc | this.ac == this.bc) {
            return true;
        }
    }
    isEquilateral() {
        if (this.exist == false) {
            return false;
        }
        if (this.ab == this.ac & this.ab == this.bc) {
            return true;
        }
    }
    isRectangle() {
        if (this.exist == false) {
            return false;
        }
        if (Math.pow(this.ab, 2) == Math.pow(this.ac, 2) + Math.pow(this.bc, 2) | Math.pow(this.ac, 2) == Math.pow(this.ab, 2) + Math.pow(this.bc, 2) | Math.pow(this.bc, 2) == Math.pow(this.ab, 2) + Math.pow(this.ac, 2)) {
            return true;
        }
    }

    getSideLength() {
        if (this.exist == false) {
            return 0;
        }
        return this.ab + this.ac + this.bc;
    }

    getSurfaceArea() {
        if (this.exist == false) {
            return 0;
        }
        let p = (this.ab + this.ac + this.bc) / 2;
        return Math.sqrt(p * (p - this.ab) * (p - this.ac) * (p - this.bc));
    }


}


class Cercle {
    constructor(rayon) {
        
        if (typeof rayon == "number" & rayon > 0) {
            this.rayon = rayon;
            this.exist = true;
        }
        else {
            this.exist = false;
        }
    }
    getExist() {
        return this.exist;
    }
    
    getPerimeter() {
        if (this.exist == false) {
            return 0;
        }
        return 2 * Math.PI * this.rayon;
    }
    getSurfaceArea() {
        if (this.exist == false) {
            return 0;
        }
        return Math.PI * this.rayon * this.rayon;
    }
}

module.exports = {
    Triangle: Triangle,
    Cube: Cube,
    Cercle: Cercle
}
